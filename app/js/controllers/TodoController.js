'use strict';
define(['app'], function(app)
{
	app.controller('TodoController',['$scope', '$http',

        function($scope, $http)
        {
            $scope.page ={
                title: 'Todo List'
            };

            $scope.taskList = [];
            $scope.checkStatus = false;
            $scope.orderBy = false;
            $scope.setOrder = true;

            function loadData(){

                $http.get('../data/todolist.json')
                .success(function(data) {
                    $scope.taskList = data;
                });

            }

            $scope.submitTask = function(){
                console.log($scope.taskName)

                if(angular.isUndefined($scope.taskName) || $scope.taskName == ''){
                    return;
                };

                $scope.taskList.push({id: $scope.taskList.length+1, name:$scope.taskName, status:'pending', done:false});
                $scope.taskName = '';
            }

            $scope.removeTask = function(index){
                $scope.taskList.splice(index,1);
            }
              
            $scope.searchTaskName = function() {
                return $scope.searchTask;   
            };

            $scope.statusComplete = function(item) {

                if(item.done){
                    item.status = 'completed'
                }else{
                    item.status = 'pending'
                }
                
            };

            $scope.orderById = function(type){
                $scope.orderBy = !$scope.orderBy;

                if($scope.orderBy){
                    $scope.setOrder = type
                }else{
                    $scope.setOrder = '-' + type
                }

            }

            loadData();      
        }
    ]);
});