define([], function()
{
    return {
        defaultRoutePath: '/',
        routes: {
            '/': {
                templateUrl: 'app/partials/todo.html',
                dependencies: [
                    'controllers/TodoController'
                ]
            }
        }
    };
});