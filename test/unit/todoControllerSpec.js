'use strict';

module('app', function(app)
{
	console.log(app)
});




define(['controllers/TodoController'], function(app)
{
	describe("The 'TodoController'", function()
	{
		var $rootScope;
		var $controller;
		var $scope;

		beforeEach(function()
		{
			module('app');

			inject
			([
				'$injector',
				'$rootScope',
				'$controller',

				function($injector, _$rootScope, _$controller)
				{
					$rootScope = _$rootScope;
					$scope = $rootScope.$new();
					$controller = _$controller;
				}
			]);

			$controller('TodoController', {$scope: $scope});
		});



		it("should set the page title to 'Todo List' ", function()
		{
			expect($scope.page.title).toBe('Todo List');
		});
	});
});